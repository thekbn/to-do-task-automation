package info.todo.automation.test;

import org.testng.annotations.Test;

public class TestTaskDelete extends BaseTest{
    /**
     *
     * @throws InterruptedException
     */
    @Test
    public void testDeleteAllTasks() throws InterruptedException {
        getToDoApp()
                .clickSelectAllCheckbox()
                .clickRemove()
                .verifyAllTasksDeleted();
    }
}
