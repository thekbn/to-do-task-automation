package info.todo.automation.test;

import info.todo.automation.utility.StringUtility;
import org.testng.annotations.Test;

public class TestTaskAdd extends BaseTest {

    /**
     * Secnario: Add Task
     * Test Case # 1. Verify a task can be added without any categories or due date
     * @throws InterruptedException
     */
    @Test
    public void testAddTaskWOCatAndDueDate() throws InterruptedException {
        String taskName = StringUtility.generateRandomSentence();
        getToDoApp()
                .typeTaskName(taskName)
                .clickAdd()
                .verifyTaskNameIsInList(taskName);
    }

}
