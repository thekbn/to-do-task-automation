package info.todo.automation.test;

import info.todo.automation.utility.StringUtility;
import org.testng.annotations.Test;

public class TestTaskModify extends BaseTest{

    /**
     * Scneario: Modify Task
     * Test Case # 1.1 Verify the task name can be modified for a task without any categories or due date
     */
    @Test
    public void testModifyTask() throws InterruptedException {
        String taskName = StringUtility.generateRandomSentence();
        getToDoApp()
                .clickTaskId()
                .typeUpdateTaskName(taskName)
                .clickUpdate()
                .verifyTaskNameIsInList(taskName);
    }
}
