package info.todo.automation.test;

import info.todo.automation.page.ToDoList;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseTest {
    private static WebDriver driver;

    @BeforeTest
    public void beforeTest() {
        System.setProperty("webdriver.gecko.driver", "geckodriver");
        this.driver = new FirefoxDriver();
    }

    @AfterTest
    public void afterTest(){
        //driver.quit();
    }

    public ToDoList getToDoApp() {
        return new ToDoList(driver, true);
    }
}

